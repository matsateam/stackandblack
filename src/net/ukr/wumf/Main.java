package net.ukr.wumf;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Black blacklist = new Black();
		Stack stack = new Stack(blacklist);

		blacklist.addClass(String.class);
		blacklist.addClass(Integer.class);

		System.out.println(blacklist);
		System.out.println(stack);

		stack.addStack(333);
		stack.addStack(12.5);
		stack.addStack(123456789076543256L);
		stack.addStack("Text");
		stack.addStack(new Scanner(System.in));
		
		System.out.println(stack);

		Object cop1 = stack.getElementFromStack();
		System.out.println(stack);

		Object cop2 = stack.getAnddelStack();
		System.out.println(stack);
		System.out.println();
		System.out.println(cop2);
		
	}

}
