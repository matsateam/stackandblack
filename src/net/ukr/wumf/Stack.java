package net.ukr.wumf;

public class Stack {
	
	private Object[] objStack;
	private Black blacklist;

	public Stack(Black blacklist) {
		super();
		objStack = new Stack[0];
		this.blacklist = blacklist;
	}

	public Stack() {
		super();
	}

	public void addStack(Object obj) {
		if (!blacklist.exists(obj)) {
			Object[] created = new Object[objStack.length + 1];
			for (int i = 0; i < objStack.length; i++) {
				created[i] = objStack[i];
			}
			objStack = created;
			for (int i = 0; i < objStack.length; i++) {
				if (objStack[i] == null) {
					objStack[i] = obj;
					System.out.println(obj.getClass() + " stack ��������.");
					return;
				}
			}
		} else {
			System.out.println(obj.getClass() + " �� ��� �������� ������ ��� ���� ����� � ������ ������.");
		}
	}

	public Object getAnddelStack() {
		if (objStack.length == 0) {
			System.out.println("Stack ������");
			return null;
		}
		System.out
				.println("\n������� ������� '" + objStack[objStack.length - 1] + "' ��� ���������� � ������ �� stack");
		Object saved = objStack[objStack.length - 1];
		Object[] created = new Object[objStack.length - 1];
		for (int i = 0; i < objStack.length - 1; i++) {
			created[i] = objStack[i];
		}
		objStack = created;
		return saved;
	}

	public Object getElementFromStack() {
		if (objStack.length == 0) {
			System.out.println("Stack ������");
			return null;
		}
		System.out.println("\n������� ������� '" + objStack[objStack.length - 1] + "' ��� ���������� �� stack");
		return objStack[objStack.length - 1];
	}

	@Override
	public String toString() {
		if (objStack.length == 0)
			return "\nStack ������\n";
		StringBuilder sb = new StringBuilder();
		sb.append("\nStack ������� ��:");
		for (Object i : objStack) {
			sb.append("\n- " + i.getClass() + " (" + i + ")");
		}
		return sb.toString();
	}
}
